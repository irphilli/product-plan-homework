import {initialData} from '../src/initial-data';
import {onDragEnd} from '../src/logic';


const addLane = (state) => {
  const dragResult = {
    destination: {
      droppableId: 'all-lanes',
    },
    source: {
      droppableId: 'addLane',
    },
    draggableId: 'lane-add',
    type: 'lane',
  };
  return onDragEnd(state, dragResult);
}

const addBar = (state, timelineId, index) => {
  const dragResult = {
    destination: {
      droppableId: timelineId,
      index: index,
    },
    source: {
      droppableId: 'addBar',
    },
    draggableId: 'bar-add',
    type: 'bar',
  };
  return onDragEnd(state, dragResult);
}

test('Add lanes', () => {
  let state = initialData;
  
  // Add a lane
  state = addLane(state);
  expect(state).toMatchSnapshot();
  
  // Add one more lane
  state = addLane(state);
  expect(state).toMatchSnapshot();
});

test('Add multiple bars', () => {
  let state = initialData;
  
  // Add some lanes
  state = addLane(state);
  state = addLane(state);
  
  // Add a bar
  state = addBar(state, 'timeline-1', 0);
  expect(state).toMatchSnapshot();
  
  // Add another bar somewhere else in the timeline
  state = addBar(state, 'timeline-1', 3);
  expect(state).toMatchSnapshot();
  
  // Add another bar to a different timeline
  state = addBar(state, 'timeline-3', 0);
  expect(state).toMatchSnapshot();
});

test('Move lane', () => {
  let state = initialData;
  
  // Add some lanes
  state = addLane(state);
  state = addLane(state);
  state = addLane(state);
  
  // Move top lane to bottom
  const dragResult = {
    destination: {
      droppableId: 'all-lanes',
      index: 2,
    },
    source: {
      droppableId: 'all-lanes',
      index: 0,
    },
    draggableId: 'lane-3',
    type: 'lane',
  };
  state = onDragEnd(state, dragResult);
  expect(state).toMatchSnapshot();
});

test('Move bars', () => {
  let state = initialData;
  
  // Add some lanes
  state = addLane(state);
  state = addLane(state);
  
  // Add some bars
  state = addBar(state, 'timeline-1', 0);
  state = addBar(state, 'timeline-1', 1);
  state = addBar(state, 'timeline-1', 3);
  state = addBar(state, 'timeline-2', 0);
  state = addBar(state, 'timeline-3', 0);
  
  // Move some bars
  
  // Same timeline
  let dragResult = {
    destination: {
      droppableId: 'timeline-1',
      index: 1,
    },
    source: {
      droppableId: 'timeline-1',
      index: 0,
    },
    draggableId: 'bar-1',
    type: 'bar',
  };
  state = onDragEnd(state, dragResult);
  expect(state).toMatchSnapshot();
  
  
  // Different timeline
  dragResult = {
    destination: {
      droppableId: 'timeline-2',
      index: 1,
    },
    source: {
      droppableId: 'timeline-1',
      index: 1,
    },
    draggableId: 'bar-1',
    type: 'bar',
  };
  state = onDragEnd(state, dragResult);
  expect(state).toMatchSnapshot();
  
  // Different timeline, different lane
  dragResult = {
    destination: {
      droppableId: 'timeline-3',
      index: 1,
    },
    source: {
      droppableId: 'timeline-2',
      index: 1,
    },
    draggableId: 'bar-1',
    type: 'bar',
  };
  state = onDragEnd(state, dragResult);
  expect(state).toMatchSnapshot();
  
  
});
