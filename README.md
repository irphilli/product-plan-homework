## Quick Access

A static build of the React app is available at https://productplan.philphillips.net

## Setup

To start the app locally, run:

```
yarn install
yarn start
```

You can now access it at: http://localhost:3000

To run tests, run: `yarn test`

## Project Notes

Utilizes [react-beautiful-dnd](https://github.com/atlassian/react-beautiful-dnd/ "react-beautiful-dnd") for the drag/drop functionality, and [react-joyride](https://github.com/gilbarbara/react-joyride "react-joyride") for the tour.

### Issues

* `react-beautiful-dnd` isn't so great when not using traditional lists. As a result, the spaces between bars are pretty much hacked together (they are elements in each list acting as placeholders). In retrospect, I would probably go with something like [react-dnd](https://github.com/react-dnd/react-dnd/ "react-dnd") over `react-beautiful-dnd`.
* Styles are very bare-bones. Spent most of the time getting functionality to work. The date line and dots are essentially smoke and mirrors. Ideally, the lane container and date line would scale together horizontally instead of being fixed width.
* Started to run out of time, so the tests are really basic. Ideally, they would mount the React app through `react-test-renderer` and run through that. I ran into issues getting `react-beautiful-dnd` to play nice with `react-test-renderer`, so in the interest of time went with tests that cover the core app logic.

### Bonus Points
* Didn't get to make it so bars can be deleted, though it would be fairly easy. Could basically have it so if the drop target is null for a bar, it is deleted. Held off on that for now since I wanted to make it a bit more clear from a UX standpoint that bars would be deleted in that fashion.
* Application *is not* responsive (see notes from above).
* You **can** drag bars into additional lanes on the timeline.
* Timeline *is not* horizontally scrollable (see notes from above).
* Lane container *is not* expandable / collapsable.
* The tour **does** get its content from an API call.  Set up https://api.myjson.com/bins/zt9fp with some example JSON to populate the tour. If the call fails, there's fallback content built into the app (basically the same headers, but appended with `(DEFAULT!)`).
